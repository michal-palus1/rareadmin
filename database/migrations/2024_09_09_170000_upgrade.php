<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Upgrade extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('entities', 'rareadmin_entities');

        Schema::table('rareadmin_entities', function (Blueprint $table) {
            $table->dropColumn('show_in_sidebar');
        });

        Schema::rename('roles', 'rareadmin_roles');

        Schema::rename('types', 'rareadmin_types');

        Schema::table('rareadmin_types', function (Blueprint $table) {
            $table->dropColumn('for_datatable');
            $table->dropColumn('for_store');
            $table->renameColumn('for_form', 'form');
        });

        Schema::rename('columns', 'rareadmin_columns');

        Schema::rename('restrictions', 'rareadmin_restrictions');

        \Illuminate\Support\Facades\DB::table('rareadmin_entities')->where('name', 'roles')->update(['name' => 'rareadmin_roles']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('rareadmin_entities', 'entities');

        Schema::table('entities', function (Blueprint $table) {
            $table->boolean('show_in_sidebar')->default(1);
        });

        Schema::rename('rareadmin_roles', 'roles');

        Schema::rename('rareadmin_types', 'types');

        Schema::table('types', function (Blueprint $table) {
            $table->longText('for_datatable')->nullable();
            $table->longText('for_store')->nullable();
            $table->renameColumn('form', 'for_form');
        });

        Schema::rename('rareadmin_columns', 'columns');

        Schema::rename('rareadmin_restrictions', 'restrictions');

        \Illuminate\Support\Facades\DB::table('entities')->where('name', 'rareadmin_roles')->update(['name' => 'roles']);
    }
}

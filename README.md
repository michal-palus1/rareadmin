# Rare Admin Panel for your Laravel Application

[![Latest Version on Packagist](https://img.shields.io/packagist/v/michal-palus1/rareadmin.svg?style=flat-square)](https://packagist.org/packages/michal-palus1/rareadmin)
[![Total Downloads](https://img.shields.io/packagist/dt/michal-palus1/rareadmin.svg?style=flat-square)](https://packagist.org/packages/michal-palus1/rareadmin)

This is where your description should go. Try and limit it to a paragraph or two, and maybe throw in a mention of what PSRs you support to avoid any confusion with users and contributors.

## Installation

Before install RareAdmin, you must have successfully set up Authentication in your Laravel App. [More info...](https://laravel.com/docs/authentication)

Or if you have not installed auth yet, you can use these commands:
```bash
composer require laravel/ui
php artisan ui bootstrap --auth
npm install && npm run dev
```

Now you can install the package via composer:

```bash
composer require michal-palus1/rareadmin
```

Now run database migration and seed DEMO data:
```bash
php artisan migrate
php artisan rareadmin:seed
```


After that, replace *public $fillable* property in default User Model (App\Models\User) with:
```php
protected $guarded = ['id'];

public function role() {
    return $this->belongsTo(\MichalPalus1\Rareadmin\Models\Role::class);
}
```

Last step, publish js and css assets, and you are done:
```php
php artisan vendor:publish --provider="MichalPalus1\Rareadmin\RareadminServiceProvider"
```

## Usage

You can create Models and Migrations classic way in your code. (php artisan make:model Test -m) with all relations and attributes you want.

After you migrations and Models are ready, you can visit RareAdmin at url: */superadmin* (for example: http://127.0.0.1:8000/superadmin). 
There you can map your Models with system Entities, with all columns and field types and restriction for different role types, to create fully customizable Admin Panel.

!!! All models must contains: 
```php
protected $guarded = ['id'];
```

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

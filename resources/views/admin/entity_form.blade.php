@extends('rareadmin::layouts.app')

@php($ent_for_assets = \MichalPalus1\Rareadmin\Models\Entity::with('columns.type')->where('name', $entities_name)->first())

@section('content')

    @if($ent_for_assets && $ent_for_assets->columns && in_array('wysiwyg', $ent_for_assets->columns->pluck('type.name')->toArray()))
        <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
    @endif

    @if($ent_for_assets && $ent_for_assets->columns && in_array('code', $ent_for_assets->columns->pluck('type.name')->toArray()))
        <link href="https://cdn.jsdelivr.net/npm/codemirror@5.65.2/lib/codemirror.css" rel="stylesheet">
    @endif

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">{{\MichalPalus1\Rareadmin\Models\Entity::where('name', $entities_name)->first()->title}} > @if($entity->id) @lang('Edit') @else @lang('Add') @endif</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Admin</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.entities', ['entities_name' => $entities_name])}}">{{\MichalPalus1\Rareadmin\Models\Entity::where('name', $entities_name)->first()->title}}</a></li>
                        <li class="breadcrumb-item active">@if($entity->id) @lang('Edit') @else @lang('Add') @endif</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">

                            <form method="post" action="{{route('admin.entity_store', ['entities_name' => $entities_name, 'id' => $entity->id])}}" enctype="multipart/form-data">
                                @csrf
                                {{getColumns($entities_name, 'form', $entity)}}
                                <button type="submit" class="btn btn-primary btn-lg">@lang('Save')</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@section('js')

    @if($ent_for_assets && $ent_for_assets->columns && in_array('wysiwyg', $ent_for_assets->columns->pluck('type.name')->toArray()))

        <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

        <script>
            $(document).ready(function() {
                $('.summernote').summernote();
            });
        </script>
    @endif

    @if($ent_for_assets && $ent_for_assets->columns && in_array('code', $ent_for_assets->columns->pluck('type.name')->toArray()))

        <script src="https://cdn.jsdelivr.net/npm/codemirror@5.65.2/lib/codemirror.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/codemirror@5.65.2/mode/clike/clike.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/codemirror@5.65.2/mode/php/php.js"></script>

        <script>
            var editor = CodeMirror.fromTextArea(document.getElementsByClassName('codemirror')[0], {
                mode: "text/x-php",
                lineNumbers: true,
                indentUnit: 4,
                indentWithTabs: true
            });
        </script>
    @endif

@endsection


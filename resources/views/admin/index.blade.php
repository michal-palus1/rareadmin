@extends('rareadmin::layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0"> @lang('Dashboard')</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Admin</a></li>
                        <li class="breadcrumb-item active">@lang('Dashboard')</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container">
            <div class="row">
                @foreach(\MichalPalus1\Rareadmin\Models\Entity::all() as $entity)
                    @if(can('view', $entity, 'Entity'))
                        <div class="col-lg-4">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title w-100">{{$entity->title}} <span class="float-right">({{get_model($entity->name)->count()}})</span></h5>
                                    <div class="pt-5">
                                        <a href="{{route('admin.entities', ['entities_name' => $entity->name])}}">
                                            <button class="btn btn-sm btn-success">@lang('Show')</button>
                                        </a>
                                        @if(auth()->user()->role->name == 'superadmin')
                                            <a href="{{route('superadmin.entity_form', $entity)}}">
                                                <button class="btn btn-sm btn-primary">@lang('Manage')</button>
                                            </a>
                                        @endif
                                        <a href="{{route('admin.entity_import', ['entities_name' => $entity->name])}}">
                                            <button class="btn btn-sm btn-secondary">@lang('Import')</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@extends('rareadmin::layouts.app')

@section('content')

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">{{\MichalPalus1\Rareadmin\Models\Entity::where('name', $entities_name)->first()->title}} > @lang('Import')</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Admin</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.entities', ['entities_name' => $entities_name])}}">{{\MichalPalus1\Rareadmin\Models\Entity::where('name', $entities_name)->first()->title}}</a></li>
                        <li class="breadcrumb-item active">@lang('Import')</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form id="import" enctype="multipart/form-data">
                                <h1>@lang('Select file'):</h1>
                                <input type="file" name="importFile" id="importFile">
                            </form>
                            <form method="POST" action="{{route('admin.entity_import_proccess', ['entities_name' => $entities_name])}}" id="import_final" style="margin-top: 40px; display: none">

                                @csrf

                                <h1>@lang('Data preview & reorder'):</h1>
                                <table class="table" id="preview_table">
                                    <thead>
                                    <tr></tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>

                                <h2 style="margin-top: 40px">@lang('Final Import settings'):</h2>

                                <div class="form-group row">
                                    <label for="input_skip_first_row" class="col-sm-4 col-form-label">@lang('Skip first row?')</label>
                                    <div class="col-sm-8">
                                        <input type="checkbox" name="skip_first_row" style="width:40px" class="form-control" id="input_skip_first_row" value="1">
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-primary btn-lg">@lang('Proccess Import')</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@section('js')
    <script>

        function get_option_html(current, all) {
            var html = '';
            $(all).each(function (index) {
                if (all[index] == current) {
                    html += '<option selected value="' + all[index] + '">' + all[index] + '</option>';
                } else {
                    html += '<option value="' + all[index] + '">' + all[index] + '</option>';
                }
            });

            return html;
        }

        $('#importFile').on('change', function () {
            $.ajax({
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();

                    xhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            percentComplete = parseInt(percentComplete * 100);

                            Swal.fire({
                                text: percentComplete+"% done.",
                                timer: 500,
                                button: false
                            });

                            if (percentComplete === 100) {
                                Swal.fire({
                                    icon: "success",
                                    timer: 1500,
                                });
                            }

                        }
                    }, false);

                    return xhr;
                },
                url: '{{route('admin.entity_import_preview', ['entities_name' => $entities_name])}}',
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: new FormData($('#import')[0]),
                cache: false,
                contentType: false,
                processData: false,
                success: function (result) {
                    $('#import_final').show();

                    $(result.data[0]).each(function (index) {
                        $('#preview_table thead tr').append('<th><select name="order[]"><option value="null">@lang('Skip')</option>'+get_option_html(result.columns[index], result.columns)+'</th>');
                    });

                    $(result.data).each(function (index) {
                        $('#preview_table tbody').append('<tr></tr>');

                        $(result.data[index]).each(function (index2) {
                            $('#preview_table tbody tr:last-child').append('<td>'+result.data[index][index2]+'</td>');
                        });
                    });
                }
            });
        });
    </script>
@endsection


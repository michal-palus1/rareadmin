@extends('rareadmin::layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0"> {{$entity->title}} @if(can('create', $entity, 'Entity')) <a href="{{route('admin.entity_form', ['entities_name' => $entities_name])}}"><button class="btn btn-success"><i class="fa fa-plus"></i> @lang('Add')</button></a>@endif</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('admin.index')}}">Admin</a></li>
                        <li class="breadcrumb-item active">{{$entity->title}}</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th></th>
                                    {{getColumns($entities_name, 'table')}}
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@section('js')
    <script>

        $('.table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            ajax: '{!! route('admin.entities_ajax', $entities_name) !!}',
            columns: [
                {{getColumns($entities_name, 'js')}}
            ],
            columnDefs: [
                {
                    render: function ( data, type, row ) {

                        var str = '';

                        @if(can('edit', $entity, 'Entity'))
                            str +=  '<a href="' + '{{route('admin.entity_form', ['entities_name' => $entities_name, 'id' => 'XXX'])}}'.replace('XXX', row['id']) + '">' +
                                        '<button class="btn btn-sm btn-primary">@lang('Edit')</button>' +
                                    '</a> ';
                        @endif

                        @if(can('delete', $entity, 'Entity'))
                            str +=  '<a href="' + '{{route('admin.entity_delete', ['entities_name' => $entities_name, 'id' => 'XXX'])}}'.replace('XXX', row['id']) + '" onclick="return confirm(\'@lang('Are you sure you want to delete this entity?')\')">' +
                                        '<button class="btn btn-sm btn-danger">@lang('Delete')</button>' +
                                    '</a>';
                        @endif

                        return str;
                    },
                    targets: 0
                }
            ]
        });

    </script>
@endsection

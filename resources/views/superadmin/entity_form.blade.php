@extends('rareadmin::layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0"> @lang('Add & Edit') @lang('Entity')</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('superadmin.index')}}">Superadmin</a></li>
                        <li class="breadcrumb-item"><a href="{{route('superadmin.entities')}}">@lang('Entities')</a></li>
                        <li class="breadcrumb-item active">@lang('Add & Edit')</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" action="{{route('superadmin.entity_store', $entity)}}">
                                @csrf

                                <div class="form-group row">
                                    <label for="input_name" class="col-sm-4 col-form-label">@lang('Name of table in DB')</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="name" class="form-control" id="input_name" value="{{$entity->name}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="input_title" class="col-sm-4 col-form-label">@lang('Title')</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="title" class="form-control" id="input_title" value="{{$entity->title}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="input_icon" class="col-sm-4 col-form-label">@lang('Icon') (<a href="https://fontawesome.com/v6/search?o=r&m=free" target="_blank">@lang('All icons')</a>)</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="icon" class="form-control" id="input_icon" value="{{$entity->icon}}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="input_show_in_top_menu" class="col-sm-4 col-form-label">@lang('Show in top menu')</label>
                                    <div class="col-sm-1">
                                        <input type="checkbox" style="width: 40px;" name="show_in_top_menu" class="form-control" id="input_show_in_top_menu" value="1" @if($entity->show_in_top_menu) checked @endif>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="input_hidden_columns" class="col-sm-4 col-form-label">@lang('Hidden columns')</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="hidden_columns" class="form-control" id="input_hidden_columns" value="{{$entity->hidden_columns}}">
                                    </div>
                                </div>

                                <br />
                                <h4>
                                    @lang('Columns')
                                    <button onclick="add_column();" type="button" class="btn btn-sm btn-success">@lang('Add')</button>
                                    <button onclick="remove_column();" type="button" class="btn btn-sm btn-danger">@lang('Delete')</button>
                                </h4>
                                <h6>{{implode(', ', \Illuminate\Support\Facades\Schema::getColumnListing($entity->name))}}</h6>
                                <i><small>@lang('Note.: To delete a column, leave its name blank')</small></i>
                                <br /><br />

                                <div id="columns">

                                    <script>var column_id = 0;</script>
                                    @php($column_id = 0)

                                    @if($entity->columns)
                                        <div class="form-group row column_row">
                                            <div class="col-sm-3">
                                                @lang('Column name (in DB)'):
                                            </div>
                                            <div class="col-sm-3">
                                                @lang('Model name (in code)'):
                                            </div>
                                            <div class="col-sm-2">
                                                @lang('Title for column'):
                                            </div>
                                            <div class="col-sm-2">
                                                @lang('Type'):
                                            </div>
                                            <div class="col-sm-2">
                                                @lang('Searchable column'):
                                            </div>
                                        </div>

                                        @foreach($entity->columns->sortBy('position') as $column)
                                            <div class="form-group row column_row">
                                                <input type="hidden" name="columns[{{$loop->index}}][id]" value="{{$column->id}}">

                                                <div class="col-sm-3">
                                                    <input type="text" name="columns[{{$loop->index}}][name]" class="form-control" value="{{$column->name}}" placeholder="@lang('Column name')">
                                                </div>
                                                <div class="col-sm-3">
                                                    <input type="text" name="columns[{{$loop->index}}][model]" class="form-control" value="{{$column->model}}" placeholder="@lang('Model name')">
                                                </div>
                                                <div class="col-sm-2">
                                                    <input type="text" name="columns[{{$loop->index}}][title]" class="form-control" value="{{$column->title}}" placeholder="@lang('Title for column')">
                                                </div>
                                                <div class="col-sm-2">
                                                    <select name="columns[{{$loop->index}}][type_id]" class="form-control type-select">
                                                        <option disabled selected>@lang('choose')...</option>
                                                        @foreach(\MichalPalus1\Rareadmin\Models\Type::orderBy('position')->get() as $type)
                                                            <option @if($column->type_id == $type->id) selected @endif value="{{$type->id}}">{{$type->title}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-sm-2">
                                                    <input type="text" name="columns[{{$loop->index}}][viewable]" class="form-control viewable-input" value="{{$column->viewable}}" placeholder="@lang('Searchable column name')">
                                                </div>
                                            </div>
                                            <script>column_id++;</script>
                                            @php($column_id++)
                                        @endforeach
                                    @endif

                                </div>

                                <div id="template" style="display: none">
                                    <div class="form-group row column_row">
                                        <input type="hidden" name="columns[XXX][id]" value="">

                                        <div class="col-sm-3">
                                            <input type="text" name="columns[XXX][name]" class="form-control" value="" placeholder="@lang('Column name')">
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="text" name="columns[XXX][model]" class="form-control" value="" placeholder="@lang('Model name')">
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="text" name="columns[XXX][title]" class="form-control" value="" placeholder="@lang('Title for column')">
                                        </div>
                                        <div class="col-sm-2">
                                            <select name="columns[XXX][type_id]" class="form-control type-select">
                                                <option disabled selected>@lang('choose')...</option>
                                                @foreach(\MichalPalus1\Rareadmin\Models\Type::orderBy('position')->get() as $type)
                                                    <option value="{{$type->id}}">{{$type->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="text" name="columns[XXX][viewable]" class="form-control viewable-input" value="" placeholder="@lang('Searchable column name')">
                                        </div>
                                    </div>
                                </div>

                                <br /><br />

                                <button class="btn btn-success btn-lg" type="submit">@lang('Save')</button>
                            </form>
                        </div>
                    </div><!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@section('js')
    <script>
        var column_id = 0;

        function add_column() {
            var html = $('#template').html().replaceAll('XXX', column_id);
            $('#columns').append(html);
            column_id++;
        }

        function remove_column() {
            $('#columns').find('.column_row').last().remove();
            column_id--;
        }

        setInterval(function () {
            $('.column_row').each(function () {
                if ($(this).find('.type-select option:selected').first().text() == '{{__('One-To-Many Relation')}}' ||
                    $(this).find('.type-select option:selected').first().text() == '{{__('Parent')}}' ||
                    $(this).find('.type-select option:selected').first().text() == '{{__('Morph')}}' ||
                    $(this).find('.type-select option:selected').first().text() == '{{__('Many-To-Many Relation')}}'
                ) {
                    $(this).find('.viewable-input').show();
                } else {
                    $(this).find('.viewable-input').hide();
                }
            });
        }, 500);

    </script>
@endsection

@extends('rareadmin::layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0"> @lang('Input types')</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('superadmin.index')}}">Superadmin</a></li>
                        <li class="breadcrumb-item active">@lang('Input types')</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
{{--                                        <th>Action</th>--}}
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach(\MichalPalus1\Rareadmin\Models\Type::orderBy('position')->get() as $type)
                                        <tr>
                                            <td>{{$type->id}}</td>
                                            <td>{{$type->title}}</td>
                                            <td>
{{--                                                <a href="{{route('superadmin.entity_form', $entity)}}">--}}
{{--                                                    <button class="btn btn-sm btn-primary">Edit</button>--}}
{{--                                                </a>--}}
{{--                                                <a href="{{route('superadmin.entity_delete', $entity)}}" onclick="return confirm('Naozaj?')">--}}
{{--                                                    <button class="btn btn-sm btn-danger">Delete</button>--}}
{{--                                                </a>--}}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div><!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@section('js')
    <script>

    </script>
@endsection

@extends('rareadmin::layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0"> @lang('Add & Edit') @lang('Permission')</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('superadmin.index')}}">Superadmin</a></li>
                        <li class="breadcrumb-item"><a href="{{route('superadmin.restrictions')}}">@lang('Permissions')</a></li>
                        <li class="breadcrumb-item active">@lang('Add & Edit')</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" action="{{route('superadmin.restriction_store', $restriction)}}">
                                @csrf

                                <div class="form-group row">
                                    <label for="input_target_entity" class="col-sm-4 col-form-label">@lang('Entity')</label>
                                    <div class="col-sm-8">
                                        <select name="target_entity" id="input_target_entity" class="form-control">
                                            <option disabled selected>@lang('choose')...</option>
                                            @foreach(\MichalPalus1\Rareadmin\Models\Entity::all() as $entity)
                                                <option @if($restriction->target_id == $entity->id && $restriction->target_type == '\MichalPalus1\Rareadmin\Models\Entity') selected @endif value="{{$entity->id}}">
                                                    {{$entity->title}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="input_target_column" class="col-sm-4 col-form-label">@lang('Column')</label>
                                    <div class="col-sm-8">
                                        <select name="target_column" id="input_target_column" class="form-control">
                                            <option value="" selected>@lang('Whole Entity')</option>
                                            @foreach(\MichalPalus1\Rareadmin\Models\Entity::with('columns')->get() as $entity)
                                                <optgroup label="{{$entity->title}}">
                                                    @foreach($entity->columns as $column)
                                                        <option @if($restriction->target_id == $column->id && $restriction->target_type == '\MichalPalus1\Rareadmin\Models\Column') selected @endif value="{{$column->id}}">
                                                            {{$column->title}} ({{$entity->title}})
                                                        </option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="input_role_id" class="col-sm-4 col-form-label">@lang('Role')</label>
                                    <div class="col-sm-8">
                                        <select name="role_id" id="input_role_id" class="form-control">
                                            <option disabled selected>@lang('choose')...</option>
                                            @foreach(\MichalPalus1\Rareadmin\Models\Role::all() as $role)
                                                <option @if($restriction->role_id == $role->id) selected @endif value="{{$role->id}}">
                                                    {{$role->name}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="input_view" class="col-sm-4 col-form-label">@lang('View records')</label>
                                    <div class="col-sm-1">
                                        <input class="form-control" type="checkbox" value="1" name="view" @if($restriction->view) checked @endif>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="input_edit" class="col-sm-4 col-form-label">@lang('Edit record')</label>
                                    <div class="col-sm-1">
                                        <input class="form-control" type="checkbox" value="1" name="edit" @if($restriction->edit) checked @endif>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="input_create" class="col-sm-4 col-form-label">@lang('Create new record')</label>
                                    <div class="col-sm-1">
                                        <input class="form-control" type="checkbox" value="1" name="create" @if($restriction->create) checked @endif>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="input_delete" class="col-sm-4 col-form-label">@lang('Delete record')</label>
                                    <div class="col-sm-1">
                                        <input class="form-control" type="checkbox" value="1" name="delete" @if($restriction->delete) checked @endif>
                                    </div>
                                </div>

                                <button class="btn btn-success btn-lg" type="submit">@lang('Save')</button>
                            </form>
                        </div>
                    </div><!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@section('js')
    <script>

    </script>
@endsection

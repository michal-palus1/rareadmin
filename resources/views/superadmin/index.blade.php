@extends('rareadmin::layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0"> Superadmin</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('superadmin.index')}}">Superadmin</a></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="text-center">
                                <h2>Welcome to RareAdmin Panel</h2>
                                <p>First, create all Laravel Models and Migrations and run the migration to create the database tables.<br />
                                    After you have correctly created all the Models and Methods in them for all Relations according to the Laravel instructions <a target="_blank" href="https://laravel.com/docs/eloquent-relationships">here</a>,<br />
                                    you can start the auto-identification process by clicking the button below and your Admin Panel will be ready.</p>
                                <a href="{{route('superadmin.identify')}}"><button class="btn btn-lg btn-primary">@lang('Auto Discover Models')</button></a>
                            </div>
                            <hr>
                            <h6>Try different roles:</h6>
                            @foreach(\MichalPalus1\Rareadmin\Models\Role::all() as $role)
                                @if($role->users && $role->users->first() && $role->name != 'superadmin')
                                    <a href="{{route('superadmin.sign_in_as', $role->users->first())}}"><button class="btn btn-sm btn-warning">@lang('Sign in as') {{$role->name}}</button></a>&nbsp; | &nbsp;
                                @endif
                            @endforeach
                        </div>
                    </div><!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@section('js')
    <script>

    </script>
@endsection

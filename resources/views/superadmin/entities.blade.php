@extends('rareadmin::layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0"> @lang('Entities') <a href="{{route('superadmin.entity_form')}}"><button class="btn btn-success"><i class="fa fa-plus"></i> @lang('Add')</button></a></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('superadmin.index')}}">Superadmin</a></li>
                        <li class="breadcrumb-item active">@lang('Entities')</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>@lang('Name')</th>
                                        <th>@lang('Action')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach(\MichalPalus1\Rareadmin\Models\Entity::all() as $entity)
                                        <tr>
                                            <td>{{$entity->id}}</td>
                                            <td><i class="fas {{$entity->icon}}"></i> {{$entity->title}}</td>
                                            <td>
                                                <a href="{{route('superadmin.entity_form', $entity)}}">
                                                    <button class="btn btn-sm btn-primary">@lang('Edit')</button>
                                                </a>
                                                <a href="{{route('superadmin.entity_delete', $entity)}}">
                                                    <button class="btn btn-sm btn-danger">@lang('Delete')</button>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div><!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@section('js')
    <script>

    </script>
@endsection

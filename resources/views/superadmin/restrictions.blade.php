@extends('rareadmin::layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0"> @lang('Permissions') <a href="{{route('superadmin.restriction_form')}}"><button class="btn btn-success"><i class="fa fa-plus"></i> @lang('Add')</button></a></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('superadmin.index')}}">Superadmin</a></li>
                        <li class="breadcrumb-item active">@lang('Permissions')</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>@lang('To Entities'):</h5>
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>@lang('Entity')</th>
                                        <th>@lang('Role')</th>
                                        <th>@lang('View records')</th>
                                        <th>@lang('Edit record')</th>
                                        <th>@lang('Create new record')</th>
                                        <th>@lang('Delete record')</th>
                                        <th>@lang('Action')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach(\MichalPalus1\Rareadmin\Models\Restriction::with('role')->where('target_type', '\MichalPalus1\Rareadmin\Models\Entity')->get() as $restriction)
                                        <tr>
                                            <td>{{$restriction->target->title}}</td>
                                            <td>{{$restriction->role->name}}</td>
                                            <td>{!! $restriction->view ? '<i style="color:green" class="fas fa-check"></i>' : '<i style="color:red" class="fas fa-times"></i>' !!}</td>
                                            <td>{!! $restriction->edit ? '<i style="color:green" class="fas fa-check"></i>' : '<i style="color:red" class="fas fa-times"></i>' !!}</td>
                                            <td>{!! $restriction->create ? '<i style="color:green" class="fas fa-check"></i>' : '<i style="color:red" class="fas fa-times"></i>' !!}</td>
                                            <td>{!! $restriction->delete ? '<i style="color:green" class="fas fa-check"></i>' : '<i style="color:red" class="fas fa-times"></i>' !!}</td>
                                            <td>
                                                <a href="{{route('superadmin.restriction_form', $restriction)}}">
                                                    <button class="btn btn-sm btn-primary">@lang('Edit')</button>
                                                </a>
                                                <a href="{{route('superadmin.restriction_delete', $restriction)}}" onclick="return confirm('@lang('Are you sure you want to delete this permission?')')">
                                                    <button class="btn btn-sm btn-danger">@lang('Delete')</button>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div><!-- /.card -->
                </div>
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>@lang('To columns'):</h5>
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>@lang('Entity')</th>
                                    <th>@lang('Column')</th>
                                    <th>@lang('Role')</th>
                                    <th>@lang('View column')</th>
                                    <th>@lang('Edit column')</th>
                                    <th>@lang('Create new record')</th>
                                    <th>@lang('Action')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach(\MichalPalus1\Rareadmin\Models\Restriction::with('role', 'target.entity')->where('target_type', '\MichalPalus1\Rareadmin\Models\Column')->get() as $restriction)
                                    <tr>
                                        <td>{{$restriction->target->entity->title}}</td>
                                        <td>{{$restriction->target->title}}</td>
                                        <td>{{$restriction->role->name}}</td>
                                        <td>{!! $restriction->view ? '<i style="color:green" class="fas fa-check"></i>' : '<i style="color:red" class="fas fa-times"></i>' !!}</td>
                                        <td>{!! $restriction->edit ? '<i style="color:green" class="fas fa-check"></i>' : '<i style="color:red" class="fas fa-times"></i>' !!}</td>
                                        <td>{!! $restriction->create ? '<i style="color:green" class="fas fa-check"></i>' : '<i style="color:red" class="fas fa-times"></i>' !!}</td>
                                        <td>
                                            <a href="{{route('superadmin.restriction_form', $restriction)}}">
                                                <button class="btn btn-sm btn-primary">@lang('Edit')</button>
                                            </a>
                                            <a href="{{route('superadmin.restriction_delete', $restriction)}}" onclick="return confirm('@lang('Are you sure you want to delete this permission?')')">
                                                <button class="btn btn-sm btn-danger">@lang('Delete')</button>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div><!-- /.card -->
                </div>
            </div>

        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@section('js')
    <script>

    </script>
@endsection

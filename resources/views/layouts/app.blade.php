<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>RareAdmin.com - Simple Laravel Admin Panel</title>
    <icon rel="icon" href="{{asset('rareadmin/favicon.ico')}}" type="image/x-icon">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.2/css/all.min.css">

    <link rel="stylesheet" href="https://cdn.datatables.net/2.0.3/css/dataTables.bootstrap4.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('rareadmin/css/adminlte.min.css')}}">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css">

    <style>
        .select2-selection {
            height: 40px !important;
        }
        .select2-container--default .select2-selection--single, .select2-container--default .select2-selection--multiple {
            background: none;
            border-color: #6c757d;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            color: white;
            padding-left: 0;
        }
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            color: black;
            padding-left: 24px;
        }
        .select2-selection--multiple .select2-search__field {
            height: 26px !important;
            padding-left: 4px !important;
            border-radius: 4px;
            border: 1px solid #aaa;
        }
        .select2-container--default .select2-results__option--selected {
            background-color: #3f474e !important;
        }
    </style>
</head>
<body class="hold-transition dark-mode">
<div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand-md navbar-dark">
        <div class="container">
            <a href="#0" class="navbar-brand" data-widget="pushmenu">
                <span class="brand-text font-weight-light">
                    <img src="https://rareadmin.com/assets/img/ra_logo.png" alt="RareAdmin Logo" style="height: 30px;">
                </span>
            </a>

            <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse order-3" id="navbarCollapse">
                <!-- Left navbar links -->
                <ul class="navbar-nav">
                    @if(auth()->check())
                        <li class="nav-item">
                            <a href="{{route('admin.index')}}" class="nav-link"><i class="nav-icon fas fa-th"></i> @lang('Dashboard')</a>
                        </li>
                    @endif
                    @foreach(\MichalPalus1\Rareadmin\Models\Entity::where('show_in_top_menu', true)->get() as $entity)
                        @if(can('view', $entity, 'Entity'))
                            <li class="nav-item">
                                <a href="{{route('admin.entities', ['entities_name' => $entity->name])}}" class="nav-link"><i class="nav-icon fas {{$entity->icon}}"></i> {{$entity->title}}</a>
                            </li>
                        @endif
                    @endforeach
                </ul>

            </div>
        </div>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="{{route('admin.index')}}" class="brand-link">
            <span class="brand-text font-weight-light">
                <img src="https://rareadmin.com/assets/img/ra_logo.png" alt="RareAdmin Logo" style="height: 30px;">
            </span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    @if(auth()->check())
                        @if(auth()->user()->role->name == 'superadmin')
                            <li class="nav-item has-treeview menu-open">
                                <a href="javascript:" class="nav-link">
                                    <i class="nav-icon fas fa-user"></i>
                                    <p>
                                        Superadmin
                                        <i class="right fas fa-angle-left"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="{{route('superadmin.index')}}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>
                                                @lang('RareAdmin Dashboard')
                                            </p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{route('superadmin.entities')}}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>@lang('Entities')</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{route('superadmin.types')}}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>@lang('Input types')</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{route('superadmin.restrictions')}}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>@lang('Permissions')</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <div class="user-panel mb-1"></div>
                        @endif
                    @else
                        <li class="nav-item">
                            <a href="{{route('login')}}" class="nav-link">
                                <i class="nav-icon fas fa-sign-in"></i>
                                <p>@lang('Log in')</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('register')}}" class="nav-link">
                                <i class="nav-icon fas fa-user-plus"></i>
                                <p>@lang('Sign up')</p>
                            </a>
                        </li>
                    @endif
                    @foreach(\MichalPalus1\Rareadmin\Models\Entity::get() as $entity)
                        @if(can('view', $entity, 'Entity'))
                            <li class="nav-item">
                                <a href="{{route('admin.entities', ['entities_name' => $entity->name])}}" class="nav-link">
                                    <i class="nav-icon fas {{$entity->icon}}"></i>
                                    <p>{{$entity->title}}</p>
                                </a>
                            </li>
                        @endif
                    @endforeach

                    <div class="user-panel"></div>

                    <li class="nav-item">
                        @if(auth()->check())
                            <small class="ml-3">@lang('Signed in as') {{auth()->user()->name}}</small>
                        @endif
                        @if(session()->exists('prev_login'))
                            <a href="{{route('admin.sign_out_as')}}" class="nav-link">
                                <i class="nav-icon fas fa-sign-out-alt"></i>
                                <p>@lang('Log out')</p>
                            </a>
                        @else
                            @if(auth()->check())
                                <a href="#" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="nav-link">
                                    <i class="nav-icon fas fa-sign-out-alt"></i>
                                    <p>@lang('Log out')</p>
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            @endif
                        @endif
                    </li>

                    <div class="user-panel mb-1"></div>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->

        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        @yield('content')
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="float-right d-none d-sm-inline">
            Rare Admin Panel v 1.1
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; {{date('Y')}} <a href="https://rareadmin.com">RareAdmin.com</a>.</strong> All rights reserved.
    </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.7.1.min.js"></script>
<!-- Bootstrap 4 -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="{{asset('rareadmin/js/adminlte.min.js')}}"></script>
{{--<script src="/js/demo.js"></script>--}}
<script src="https://cdn.datatables.net/2.0.3/js/dataTables.min.js"></script>
<script src="https://cdn.datatables.net/2.0.3/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

<script>
    @if(session()->has('success'))

    Swal.fire({
        title: "{{session()->get('success')}}",
        icon: "success",
        timer: 1500,
    });

    @endif


    @if(session()->has('error'))

    Swal.fire({
        title: "{{session()->get('error')}}",
        icon: "error",
    });

    @endif

    $(document).ready(function() {
        $('.select2').select2();
    });
</script>

@yield('js')

</body>
</html>

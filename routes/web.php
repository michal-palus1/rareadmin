<?php

use Illuminate\Support\Facades\Route;

use MichalPalus1\Rareadmin\Http\Controllers\UniversalController;
use MichalPalus1\Rareadmin\Http\Controllers\SuperAdminController;

Route::name('admin.')->prefix('admin')->group(function () {

    Route::get('/', [UniversalController::class, 'index'])->name('index');

    Route::get('/sign_out_as', [UniversalController::class, 'sign_out_as'])->name('sign_out_as');

    Route::get('/ajax/{entities_name}', [UniversalController::class, 'entities_ajax'])->name('entities_ajax');
    Route::get('/{entities_name}', [UniversalController::class, 'entities'])->name('entities');
    Route::get('/{entities_name}/form/{id?}', [UniversalController::class, 'entity_form'])->name('entity_form');
    Route::post('/{entities_name}/store/{id?}', [UniversalController::class, 'entity_store'])->name('entity_store');
    Route::get('/{entities_name}/delete/{id}', [UniversalController::class, 'entity_delete'])->name('entity_delete');
    Route::get('/{entities_name}/import', [UniversalController::class, 'entity_import'])->name('entity_import');
    Route::post('/{entities_name}/import_preview', [UniversalController::class, 'entity_import_preview'])->name('entity_import_preview');
    Route::post('/{entities_name}/import_proccess', [UniversalController::class, 'entity_import_proccess'])->name('entity_import_proccess');

});


Route::name('superadmin.')->prefix('superadmin')->group(function () {

    Route::get('/', [SuperAdminController::class, 'index'])->name('index');
    Route::get('/sign_in_as/{user}', [SuperAdminController::class, 'sign_in_as'])->name('sign_in_as');

    Route::get('/entities', [SuperAdminController::class, 'entities'])->name('entities');
    Route::get('/entity/form/{entity?}', [SuperAdminController::class, 'entity_form'])->name('entity_form');
    Route::post('/entity/store/{entity?}', [SuperAdminController::class, 'entity_store'])->name('entity_store');
    Route::get('/entity/delete/{entity}', [SuperAdminController::class, 'entity_delete'])->name('entity_delete');


    Route::get('/types', [SuperAdminController::class, 'types'])->name('types');


    Route::get('/restrictions', [SuperAdminController::class, 'restrictions'])->name('restrictions');
    Route::get('/restriction/form/{restriction?}', [SuperAdminController::class, 'restriction_form'])->name('restriction_form');
    Route::post('/restriction/store/{restriction?}', [SuperAdminController::class, 'restriction_store'])->name('restriction_store');
    Route::get('/restriction/delete/{restriction}', [SuperAdminController::class, 'restriction_delete'])->name('restriction_delete');


    Route::get('/identify', [SuperAdminController::class, 'identify'])->name('identify');

});

<?php

namespace MichalPalus1\Rareadmin\Http\Middleware;

use Closure;

class IsSuperAdmin
{
    public function handle($request, Closure $next)
    {
        if (!$request->user()->role) {
            return redirect()->route('admin.index');
        }
        if ($request->user()->role->name !== 'superadmin') {
            return redirect()->route('admin.index');
        }

        return $next($request);
    }
}

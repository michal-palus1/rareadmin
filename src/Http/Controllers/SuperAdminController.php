<?php

namespace MichalPalus1\Rareadmin\Http\Controllers;

use MichalPalus1\Rareadmin\Http\Middleware\IsSuperAdmin;
use MichalPalus1\Rareadmin\Models\Column;
use MichalPalus1\Rareadmin\Models\Entity;
use MichalPalus1\Rareadmin\Models\Restriction;
use App\Models\User;
use Illuminate\Http\Request;

class SuperAdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['web', 'auth', IsSuperAdmin::class]);
    }

    public function index(Request $request) {
        return view('rareadmin::superadmin.index');
    }

    public function identify(Request $request) {

        $i = 0;
        foreach (get_models() as $fullModelPath) {

            $modelName = substr($fullModelPath, strrpos($fullModelPath, "/")+1);
            $model = get_model($modelName);
            $modelTable = $model->getTable();
            $column_names = [];

            if(Entity::where('name', $modelTable)->first()) {
                continue;
            }

            if (in_array($modelName, ['Column', 'Entity', 'Restriction', 'Type'])) {
                continue;
            }

            $entity = new Entity();
            $entity->name = $modelTable;
            $entity->title = $modelName;
            $entity->icon = 'fa-box-open';
            $entity->show_in_top_menu = $i < 6 ? true : false;
            $entity->hidden_columns = null;
            $entity->save();

            foreach (\Illuminate\Support\Facades\Schema::getColumnListing($modelTable) as $column_name) {

                $column_names[] = $column_name;

                if ($column_name !== 'id') {

                    $column = new Column();
                    $column->entity_id = $entity->id;
                    $column->name = $column_name;
                    $column->model = null;
                    $column->title = ucfirst(str_replace("_", " ", $column_name));
                    $column->viewable = null;

                    $type = \Illuminate\Support\Facades\Schema::getColumnType($modelTable, $column_name);
                    if ($type == 'text' || $type == 'string') {
                        $column->type_id = 1;
                    } elseif ($column_name == 'parent_id') {
                        $column->title = 'Parent';
                        $column->type_id = 6;
                        if (in_array("title", $column_names)) {
                            $column->viewable = 'title';
                        } elseif (in_array("email", $column_names)) {
                            $column->viewable = 'email';
                        } else {
                            $column->viewable = 'name';
                        }
                    } elseif ($type == 'mediumText' || $type == 'longText') {
                        $column->type_id = 2;
                    } elseif ($type == 'boolean' || $type == 'tinyInt') {
                        $column->type_id = 5;
                    } elseif (!str_contains($column_name, '_id') && (str_contains($type, 'int') || str_contains($type, 'double') || str_contains($type, 'float') || str_contains($type, 'decimal'))) {
                        $column->type_id = 11;
                    } elseif (str_contains($type, 'date') || str_contains($type, 'time') || str_contains($type, 'datetime') || str_contains($type, 'timestamp')) {
                        $column->type_id = 12;
                    } elseif (str_contains($column_name, '_id')) {
                        $column->title = str_replace(" id", "", $column->title);
                        $column->model = $column->title;
                        $column->type_id = 4;

                        $related_model_columns = \Illuminate\Support\Facades\Schema::getColumnListing(get_model($column->title)->getTable());
                        if (in_array('title', $related_model_columns)) {
                            $column->viewable = 'title';
                        } elseif (in_array("email", $related_model_columns)) {
                            $column->viewable = 'email';
                        } else {
                            $column->viewable = 'name';
                        }

                    }

                    $column->save();

                }
            }

            //$code = file_get_contents($fullModelPath.".php");

        }

        return redirect()->route('superadmin.index')->with(['success' => __('Models identified successfully')]);
    }

    public function sign_in_as(Request $request, User $user) {

        session()->put('prev_login', auth()->user()->id);
        auth()->loginUsingId($user->id);

        return redirect()->route('admin.index');
    }



    public function entities(Request $request) {
        return view('rareadmin::superadmin.entities');
    }

    public function entity_form(Request $request, Entity $entity) {
        return view('rareadmin::superadmin.entity_form', ['entity' => $entity]);
    }

    public function entity_store(Request $request, Entity $entity) {

        $entity->name = $request->name;
        $entity->title = $request->title;
        $entity->icon = $request->icon;
        $entity->show_in_top_menu = $request->show_in_top_menu ?? false;
        $entity->hidden_columns = $request->hidden_columns;

        $entity->save();

        foreach ($request->columns as $key => $column_array) {

            if(is_numeric($key)) {

                if (is_null($column_array['name']) && !is_null($column_array['id'])) {
                    Column::find($column_array['id'])->delete();
                    continue;
                }

                $column = Column::updateOrCreate(['id' => $column_array['id']],
                    [
                        'entity_id' => $entity->id,
                        'name' => $column_array['name'],
                        'model' => $column_array['model'],
                        'title' => $column_array['title'],
                        'type_id' => $column_array['type_id'],
                        'viewable' => $column_array['viewable']
                    ]
                );

            }

        }

        return redirect()->back();
    }

    public function entity_delete(Request $request, Entity $entity)
    {
        try {
            $entity->delete();
        } catch (\Exception $e) {
            return redirect()->back()->with(['error' => __('Cannot delete item')]);
        }

        return redirect()->back()->with(['success' => __('Item was deleted')]);
    }




    public function types(Request $request) {
        return view('rareadmin::superadmin.types');
    }




    public function restrictions(Request $request) {
        return view('rareadmin::superadmin.restrictions');
    }

    public function restriction_form(Request $request, Restriction $restriction) {
        return view('rareadmin::superadmin.restriction_form', ['restriction' => $restriction]);
    }

    public function restriction_store(Request $request, Restriction $restriction) {

        if ($request->target_column) {
            $restriction->target_type = '\MichalPalus1\Rareadmin\Models\Column';
            $restriction->target_id = $request->target_column;
        } else {
            $restriction->target_type = '\MichalPalus1\Rareadmin\Models\Entity';
            $restriction->target_id = $request->target_entity;
        }
        $restriction->role_id = $request->role_id;
        $restriction->view = $request->view ?? false;
        $restriction->edit = $request->edit ?? false;
        $restriction->create = $request->create ?? false;
        $restriction->delete = $request->delete ?? false;

        $restriction->save();

        return redirect()->route('superadmin.restrictions');
    }

    public function restriction_delete(Request $request, Restriction $restriction)
    {
        try {
            $restriction->delete();
        } catch (\Exception $e) {
            return redirect()->back()->with(['error' => __('Cannot delete item')]);
        }

        return redirect()->back()->with(['success' => __('Item was deleted')]);
    }
}

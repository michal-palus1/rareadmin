<?php

namespace MichalPalus1\Rareadmin\Http\Controllers;

use Illuminate\Http\Request;
use MichalPalus1\Rareadmin\Models\Entity;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use Yajra\DataTables\Facades\DataTables;
use Intervention\Image\Facades\Image;

class UniversalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['web','auth']);
    }

    public function index() {
        return view('rareadmin::admin.index');
    }

    public function sign_out_as() {
        if (session()->exists('prev_login')) {
            auth()->loginUsingId(session()->get('prev_login'));
            session()->remove('prev_login');
        }

        return redirect()->route('superadmin.index');
    }



    public function entities_ajax($entities_name) {

        $model = get_model($entities_name)->where('id', '>', 0);

        $datatable = DataTables::eloquent($model);

        $entity = Entity::where('name', $entities_name)->first();

        if(!can('view', $entity, 'Entity')) {
            exit;
        }

        foreach ($entity->columns as $column) {

            if ($column->type->name == 'relation') {

                $relation_name = str_replace('_id', '', $column->name);
                $model->with($relation_name);

                $datatable->addColumn($relation_name, function ($entity) use ($column, $relation_name) {
                    if ($entity->getAttribute($relation_name)) {
                        return $entity->getAttribute($relation_name)->getAttribute($column->viewable);
                    }
                });

            } elseif ($column->type->name == 'complex_relation') {

                $relation_name = $column->name;
                $model->with($relation_name);

                $datatable->addColumn($relation_name.'_viewable', function ($entity) use ($column, $relation_name) {
                    if ($entity->getAttribute($relation_name)) {
                        $str = '';
                        foreach ($entity->getAttribute($relation_name) as $relation) {
                            $str .= $relation->getAttribute($column->viewable). ', ';
                        }
                        return strlen($str) > 2 ? substr($str, 0, -2) : null;
                    }
                });

            } elseif ($column->type->name == 'morph') {

                $relation_name = $column->name;
                $model->with($relation_name);

                $datatable->addColumn($relation_name.'_viewable', function ($entity) use ($column, $relation_name) {
                    if ($entity->getAttribute($relation_name)) {
                        return "(".str_replace("App\\Models\\", "", get_class($entity->getAttribute($relation_name))).") ".$entity->getAttribute($relation_name)->getAttribute($column->viewable);
                    }
                });

            }elseif ($column->type->name == 'parent') {

                $datatable->addColumn('parent', function ($entity) use ($column) {
                    if ($entity->getAttribute('parent')) {
                        return $entity->getAttribute('parent')->getAttribute($column->viewable);
                    }
                });

            }

        }

        return $datatable->toJson();
    }

    public function entities($entities_name)
    {
        $entity = Entity::where('name', $entities_name)->first();
        if(!can('view', $entity, 'Entity')) {
            return redirect()->back()->with(['error' => __('You don\'t have access to module ').$entities_name]);
        }

        return view('rareadmin::admin.entities', ['entity' => $entity, 'entities_name' => $entities_name]);
    }

    public function entity_form(Request $request, $entities_name, $id = null)
    {
        if ($id) {
            $entity = get_model($entities_name)::find($id);

            $model = Entity::where('name', $entities_name)->first();
            if(!can('edit', $model, 'Entity')) {
                return redirect()->back()->with(['error' => __('You don\'t have access to module ').$entities_name]);
            }
        } else {
            $entity = get_model($entities_name);

            $model = Entity::where('name', $entities_name)->first();
            if(!can('create', $model, 'Entity')) {
                return redirect()->back()->with(['error' => __('You don\'t have access to module ').$entities_name]);
            }
        }

        return view('rareadmin::admin.entity_form', ['entity' => $entity, 'entities_name' => $entities_name]);
    }

    public function entity_store(Request $request, $entities_name, $id = null)
    {
        if ($id) {
            $entity = get_model($entities_name)::find($id);

            $model = Entity::where('name', $entities_name)->first();
            if(!can('edit', $model, 'Entity')) {
                return redirect()->back()->with(['error' => __('You don\'t have access to module ').$entities_name]);
            }
        } else {
            $entity = get_model($entities_name);

            $model = Entity::where('name', $entities_name)->first();
            if(!can('create', $model, 'Entity')) {
                return redirect()->back()->with(['error' => __('You don\'t have access to module ').$entities_name]);
            }
        }

        //get data from request
        $data = $request->except('_token', 'id');

        //handle booleans
        foreach($model->columns()->whereHas('type', function ($q) {$q->where('rareadmin_types.name', 'boolean');})->get() as $boolean) {
            if (!isset($data[$boolean->name])) {
                $data[$boolean->name] = false;
            }
        }

        //handle files
        if($request->allFiles()) {
            foreach($request->allFiles() as $input_name => $uploadedFiles) {
                $loop = 0;
                foreach ($uploadedFiles as $uploadedFile) {
                    if (sizeof($request->allFiles()[$input_name]) > 1) {
                        if ($loop == 0) {
                            $data[$input_name] = [];
                            $loop++;
                        }
                        $data[$input_name][] = $uploadedFile->storePublicly('files', 'public');

                        $imageExtensions = ['jpeg', 'jpg', 'webp', 'png', 'gif'];
                        if (in_array($uploadedFile->getClientOriginalExtension(), $imageExtensions)) {
                            $image = Image::make(storage_path("app/public/" . last($data[$input_name])));
                            $image->resize(180, 180, function ($constraint) {
                                $constraint->aspectRatio();
                                $constraint->upsize();
                            });
                            $image->save(storage_path('app/public/' . str_replace(".", "-small.", last($data[$input_name]))));
                        }
                    } else {
                        $data[$input_name] = $uploadedFile->storePublicly('files', 'public');

                        $imageExtensions = ['jpeg', 'jpg', 'webp', 'png', 'gif'];
                        if (in_array($uploadedFile->getClientOriginalExtension(), $imageExtensions)) {
                            $image = Image::make(storage_path("app/public/" . $data[$input_name]));
                            $image->resize(180, 180, function ($constraint) {
                                $constraint->aspectRatio();
                                $constraint->upsize();
                            });
                            $image->save(storage_path('app/public/' . str_replace(".", "-small.", $data[$input_name])));
                        }
                    }
                }
            }
            //workaround for uploading more files for first time
            if (is_array($data[$input_name])) {
                $data[$input_name] = json_encode($data[$input_name]);
            }
        }

        //handle polymorphic relations
        foreach($model->columns()->whereHas('type', function ($q) {$q->where('rareadmin_types.name', 'morph');})->get() as $morph) {
            $relation = $morph->name;
            if (!isset($data[$morph->name])) {
                $data[$relation.'_id'] = null;
                $data[$relation.'_type'] = null;
            } else {
                $morph_type = explode("XXX", $data[$morph->name])[0];
                $morph_id = explode("XXX", $data[$morph->name])[1];

                $data[$relation.'_id'] = $morph_id;
                $data[$relation.'_type'] = "App\\Models\\".$morph_type;
            }
        }

        //store to DB
        $entity = $entity::updateOrCreate(['id' => $id], $data);

        //handle many-to-many relations
        foreach($model->columns()->whereHas('type', function ($q) {$q->where('rareadmin_types.name', 'complex_relation');})->get() as $complex_relation) {
            $relation = $complex_relation->name;
            if (!isset($data[$complex_relation->name])) {
                $entity->$relation()->sync([]);
            } else {
                $entity->$relation()->sync($data[$complex_relation->name]);
            }
        }

        return redirect()->route('admin.entities', ['entities_name' => $entities_name])->with(['success' => __('Successfully saved')]);
    }

    public function entity_delete(Request $request, $entities_name, $id)
    {
        $model = Entity::where('name', $entities_name)->first();
        if(!can('delete', $model, 'Entity')) {
            return redirect()->back()->with(['error' => __('You don\'t have access to module ').$entities_name]);
        }

        try {
            get_model($entities_name)::find($id)->delete();
        } catch (\Exception $e) {
            return redirect()->back()->with(['error' => __('Cannot delete item')]);
        }

        return redirect()->back()->with(['success' => __('Item was deleted')]);
    }

    public function entity_import(Request $request, $entities_name)
    {
        $entity = Entity::where('name', $entities_name)->first();
        if(!can('view', $entity, 'Entity')) {
            return redirect()->back()->with(['error' => __('You don\'t have access to module ').$entities_name]);
        }

        return view('rareadmin::admin.entity_import', ['entity' => $entity, 'entities_name' => $entities_name]);
    }

    public function entity_import_preview(Request $request, $entities_name)
    {
        $entity = Entity::where('name', $entities_name)->first();
        if(!can('create', $entity, 'Entity')) {
            return redirect()->back()->with(['error' => __('You don\'t have access to module ').$entities_name]);
        }

        $uploadedFile = $request->file('importFile');

        if ($uploadedFile->extension() == 'csv' || $uploadedFile->extension() == 'txt') {
            $reader = new Csv();
        } elseif($uploadedFile->extension() == 'xls') {
            $reader = new Xls();
        } else {
            $reader = new Xlsx();
        }

        $spreadsheet = $reader->load($uploadedFile);
        $worksheet = $spreadsheet->getActiveSheet();
        $worksheet_arr = $worksheet->toArray();

        session()->put('import_data', $worksheet_arr);

        return response()->json(['columns' => $entity->columns()->pluck('name'), 'data' => array_slice($worksheet_arr, 0, 5, true)]);

    }

    public function entity_import_proccess(Request $request, $entities_name)
    {
        $entity = Entity::where('name', $entities_name)->first();
        if(!can('create', $entity, 'Entity')) {
            return redirect()->back()->with(['error' => __('You don\'t have access to module ').$entities_name]);
        }

        //check if string or int before handle
        $check_relations = [];
        foreach($entity->columns()->whereHas('type', function ($q) {$q->where('rareadmin_types.name', 'relation');})->get() as $relation) {
            $check_relations[$relation->name] = $relation;
        }
        $check_complex_relations = [];
        foreach($entity->columns()->whereHas('type', function ($q) {$q->where('rareadmin_types.name', 'complex_relation');})->get() as $complex_relation) {
            $check_complex_relations[$complex_relation->name] = $complex_relation;
        }

        $i = 0;
        foreach (session()->get('import_data') as $array) {

            try {

                $i++;
                $item = [];

                if ($i == 1 && $request->skip_first_row) {
                    continue;
                }

                foreach ($array as $key => $value) {
                    if ($request->order[$key] !== 'null') {

                        if (in_array($request->order[$key], array_keys($check_relations)) && !is_numeric($value) && is_string($value)) {
                            $item[$request->order[$key]] = get_model($check_relations[$request->order[$key]]->model)::where($check_relations[$request->order[$key]]->viewable, $value)->first()->id;
                        } elseif (in_array($request->order[$key], array_keys($check_complex_relations))) {

                            if (strpos($value, ';') !== false) {

                                $items = explode(";", $value);
                                $items = array_map('trim', $items);
                                $value = $items;

                            } else {
                                $value = [$value];
                            }

                            foreach ($value as $key2 => $element) {
                                if (is_numeric($element) && floatval($element) == intval($element)) {
                                    $item[$request->order[$key]][$key2] = intval($element);
                                } elseif (is_string($element)) {
                                    $item[$request->order[$key]][$key2] = get_model($check_complex_relations[$request->order[$key]]->model)::where($check_complex_relations[$request->order[$key]]->viewable, $element)->first()->id;
                                }
                            }

                        } elseif (is_numeric($value) && floatval($value) == intval($value)) {
                            $item[$request->order[$key]] = intval($value);
                        } else {
                            $item[$request->order[$key]] = $value;
                        }

                    }
                }

                $r = new Request($item);

                $this->entity_store($r, $entities_name, null);

            } catch (\Exception $e) {
                return redirect()->route('admin.entities', ['entities_name' => $entities_name])->with(['error' => $e->getMessage()]);
            }
        }

        return redirect()->route('admin.entities', ['entities_name' => $entities_name])->with(['success' => __('Successfully saved')]);

    }
}

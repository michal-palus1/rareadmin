<?php

namespace MichalPalus1\Rareadmin;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use MichalPalus1\Rareadmin\Console\RareadminSeed;
use MichalPalus1\Rareadmin\Http\Middleware\IsSuperAdmin;

class RareadminServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        /*
         * Optional methods to load your package assets
         */
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'rareadmin');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'rareadmin');
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');

        $router = $this->app->make(Router::class);
        $router->aliasMiddleware('IsSuperAdmin', IsSuperAdmin::class);

        if ($this->app->runningInConsole()) {

            // Publishing assets.
            $this->publishes([
                __DIR__.'/../resources/assets' => public_path('rareadmin'),
            ], 'assets');

            // Registering package commands.
            $this->commands([
                RareadminSeed::class
            ]);
        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'database');

        // Register the main class to use with the facade
        $this->app->singleton('rareadmin', function () {
            return new Rareadmin;
        });
    }
}

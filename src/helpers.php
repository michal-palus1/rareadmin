<?php

if (! function_exists('get_models')) {

    function get_models() {

        $path = app_path() . "/Models";
        $out = [];
        $results = scandir($path);
        foreach ($results as $result) {
            if ($result === '.' or $result === '..') continue;
            $filename = $path . '/' . $result;
            if (is_dir($filename)) {
                $out = array_merge($out, get_models($filename));
            }else{
                $out[] = substr($filename,0,-4);
            }
        }
        return $out;

    }

}

if (! function_exists('get_model')) {

    function get_model($entities_name) {

        if($entities_name == 'Role' || $entities_name == 'rareadmin_roles') {
            return app('\MichalPalus1\Rareadmin\Models\Role');
        } else {
            try {
                return app('App\\Models\\' . ucfirst(\Illuminate\Support\Str::singular(\Illuminate\Support\Str::camel($entities_name))));
            } catch (\Exception $e) {
                $related_model_name = \MichalPalus1\Rareadmin\Models\Column::where('name', 'like', $entities_name.'%')->first();
                return app('App\\Models\\' . ucfirst(\Illuminate\Support\Str::singular(\Illuminate\Support\Str::camel($related_model_name->model))));
            }
        }

    }

}


if (! function_exists('can')) {

    function can($type, $entity, $target) {

        if (auth()->check() && auth()->user()->role && auth()->user()->role->name != 'superadmin') {
            $restriction = \MichalPalus1\Rareadmin\Models\Restriction::where('role_id', auth()->user()->role_id)
                ->where('target_type', '\MichalPalus1\Rareadmin\Models\\' . $target)
                ->where('target_id', $entity->id)
                ->pluck($type)
                ->first();

            if ($restriction === 0 || ($target == 'Entity' && is_null($restriction))) {
                return false;
            } else {
                return true;
            }
        } elseif(auth()->check() && auth()->user()->role && auth()->user()->role->name == 'superadmin') {
            return true;
        } else {
            return false;
        }

    }

}


if (! function_exists('type_modifier_for_js')) {

    function type_modifier_for_js($column, $model) {

        $str = '';
        $col = $model->columns->where('name', $column)->first();

        if ($col && $col->type->name == 'relation') {

            $related_model = str_replace('_id', '', $column);
            $str .= "{data:'".$related_model."', name:'".$related_model.".".$col->viewable."', orderable: false},";

        } elseif ($col && $col->type->name == 'complex_relation') {

            $related_model = $column;
            $str .= "{data:'".$related_model."_viewable', name:'".$related_model.".".$col->viewable."', orderable: false},";

        } elseif ($col && $col->type->name == 'morph') {

            $related_model = $column;
            $str .= "{data:'".$related_model."_viewable', name:'".$related_model.".".$col->viewable."', orderable: false},";

        } elseif ($col && $col->type->name == 'parent') {

            $related_model = str_replace('_id', '', $column);
            $str .= "{data:'".$related_model."', name:'".$related_model.".".$col->viewable."', searchable: false, orderable: false},";

        } elseif ($col && $col->type->name == 'image') {

            $str .= "{data:'" . $column . "',name:'" . $column . "', searchable: false, render: function (data, type, row, meta) {
                if (data !== null && data.indexOf('[') !== -1 && data.indexOf('.') !== -1) {
                    data = JSON.parse(data.replace(/&quot;/g,'\"'));
                    var str = '';
                    for (const key in data) {
                        str += '<img style=\"display: inline-block; margin: 2px\" src=\"/storage/' + data[key].replace('.', '-small.') + '\" height=\"30\"/>'
                    };
                    return str;
                } else if (data !== null && data.indexOf('.') !== false) {
                    return '<img src=\"/storage/' + data.replace('.', '-small.') + '\" height=\"30\"/>';
                } else {
                    return '<img src=\"/storage/' + data + '\" height=\"30\"/>';
                }
              }},";

        } elseif ($col && $col->type->name == 'textarea') {

            $str .= "{data:'" . $column . "',name:'" . $column . "', render: function (data, type, row, meta) {
                return (typeof data == 'string' && data.length > 40) ? data.substring(0,40) + '...' : data;
              }},";

        } elseif ($col && $col->type->name == 'code') {

            $str .= "{data:'" . $column . "',name:'" . $column . "', render: function (data, type, row, meta) {
                return (typeof data == 'string' && data.length > 40) ? data.substring(0,40) + '...' : data;
              }},";

        } elseif ($col && $col->type->name == 'wysiwyg') {

            $str .= "{data:'" . $column . "',name:'" . $column . "', render: function (data, type, row, meta) {
                return (typeof data == 'string' && data.length > 40) ? data.substring(0,40) + '...' : data;
              }},";

        } elseif ($col && $col->type->name == 'boolean') {

            $str .= "{data:'" . $column . "',name:'" . $column . "', render: function (data, type, row, meta) {
                return data == 1 ? '<i class=\"fas fa-check\" style=\"color:green\">' : '<i class=\"fas fa-times\" style=\"color:red\">';
              }},";

        } else {
            $str .= "{data:'" . $column . "',name:'" . $column . "'},";
        }

        return $str;

    }

}


if (! function_exists('type_modifier_for_form')) {

    function type_modifier_for_form($column, $model, $entity) {

        if($modified_column = $model->columns->where('name', $column)->first()) {
            $column_title = $modified_column->title;
        } else {
            $column_title = $column;
        }

        $col = $model->columns->where('name', $column)->first();

        if ($col && $col->type->name == 'relation') {

            $related_model = get_model(ucfirst(str_replace('_id', '', $column)));

            $value = "<option value='' selected>-</option>";

            foreach ($related_model::all() as $related_item) {

                $selected = isset($entity->getAttributes()[$column]) && $related_item->id == $entity->getAttributes()[$column] ? 'selected' : '';

                $value .= '<option '.$selected.' value="' . $related_item->id . '">
                            ' . $related_item->getAttributes()[$model->columns->where('name', $column)->first()->viewable] . '
                           </option>';

            }

        } elseif ($col && $col->type->name == 'complex_relation') {

            $related_model = get_model(ucfirst($column));

            $value = "";

            foreach ($related_model::all() as $related_item) {

                $selected = isset($entity->$column) && in_array($related_item->id, $entity->$column->pluck('id')->toArray()) ? 'selected' : '';

                $value .= '<option '.$selected.' value="' . $related_item->id . '">
                            ' . $related_item->getAttributes()[$model->columns->where('name', $column)->first()->viewable] . '
                           </option>';

            }

        } elseif ($col && $col->type->name == 'morph') {

            $related_models = [];
            foreach(get_models() as $modelName) {
                $str = file_get_contents($modelName.".php");
                $str = preg_replace('/\s+/', '', $str);

                if(str_contains($str, "this->morph") && str_contains($str, "function".$model->name."(){") && str_contains($str, $column)) {
                    $related_models[] = substr($modelName, strrpos($modelName, "/")+1);
                }
            }

            $value = "";

            foreach ($related_models as $related_model_name) {

                $related_model = get_model(ucfirst($related_model_name));

                foreach ($related_model::all() as $related_item) {

                    $selected = isset($entity->$column->id) && $related_item->id == $entity->$column->id && get_class($related_item) == get_class($entity->$column) ? 'selected' : '';

                    $value .= '<option ' . $selected . ' value="' . $related_model_name.'XXX'.$related_item->id . '">
                           (' . $related_model_name . ') ' . $related_item->getAttributes()[$model->columns->where('name', $column)->first()->viewable] . '
                           </option>';

                }

            }

        } elseif ($col && $col->type->name == 'parent') {

            $value = "<option value='' selected>-</option>";

            foreach (get_model($model->name)::all() as $related_item) {

                $selected = isset($entity->getAttributes()[$column]) && $related_item->id == $entity->getAttributes()[$column] ? 'selected' : '';

                $value .= '<option '.$selected.' value="' . $related_item->id . '">
                            ' . $related_item->getAttributes()[$model->columns->where('name', $column)->first()->viewable] . '
                           </option>';

            }

        } elseif ($col && $col->type->name == 'boolean') {

            $value = isset($entity->getAttributes()[$column]) && $entity->getAttributes()[$column] == 1 ? ' checked' : '';

        }else {

            $value = ($entity->getAttributes()[$column] ?? '');

        }

        if ($col) {
            $str = $col->type->form;
            $str = str_replace("X_COLUMN_NAME_X", $column, $str);
            $str = str_replace("X_COLUMN_TITLE_X", $column_title, $str);
            $str = str_replace("X_VALUE_X", $value, $str);
        } else {
            $str = \MichalPalus1\Rareadmin\Models\Type::first()->form;
            $str = str_replace("X_COLUMN_NAME_X", $column, $str);
            $str = str_replace("X_COLUMN_TITLE_X", $column_title, $str);
            $str = str_replace("X_VALUE_X", $value, $str);
        }

        return $str;

    }

}

if (! function_exists('getColumns')) {

    function getColumns($table, $for = null, $entity = null) {

        $model = \MichalPalus1\Rareadmin\Models\Entity::with('columns')->where('name', $table)->first();

        $str = "";
        if ($for == 'js') {

            //get all table columns from DB
            $columns = \Illuminate\Support\Facades\Schema::getColumnListing($table);
            foreach ($columns as $column) {

                //if is not in hidden columns
                if (!in_array($column, explode(',', preg_replace('/\s+/', '', $model->hidden_columns)))) {

                    //permission check
                    if(is_null($model->columns->where('name', $column)->first())) {
                        $str .= type_modifier_for_js($column, $model);
                    } elseif(can('view', $model->columns->where('name', $column)->first(), 'Column')) {
                        $str .= type_modifier_for_js($column, $model);
                    }

                }

            }

            //append DB table columns with many-to-many relations
            foreach ($model->columns()->whereHas('type', function ($q) {$q->whereIn('rareadmin_types.name', ['complex_relation', 'relation', 'morph']);})->get() as $complex_relation) {

                //if is not in hidden columns or in columns as relation
                if (
                    !in_array($complex_relation->name, array_merge(['id'], explode(',', preg_replace('/\s+/', '', $model->hidden_columns))))
                    and
                    !in_array($complex_relation->name, $columns)
                ) {

                    //permission check
                    if(is_null($model->columns->where('name', $complex_relation->name)->first())) {
                        $str .= type_modifier_for_js($complex_relation->name, $model);
                    } elseif(can('view', $model->columns->where('name', $complex_relation->name)->first(), 'Column')) {
                        $str .= type_modifier_for_js($complex_relation->name, $model);
                    }
                }

            }

            $str = substr($str, 0,-1);

        } elseif ($for == 'form') {

            //get all table columns from DB
            $columns = \Illuminate\Support\Facades\Schema::getColumnListing($table);
            foreach ($columns as $column) {

                //if is not in hidden columns
                if (!in_array($column, array_merge(['id'], explode(',', preg_replace('/\s+/', '', $model->hidden_columns))))) {

                    //permission check
                    if(is_null($model->columns->where('name', $column)->first())) {
                        $str .= type_modifier_for_form($column, $model, $entity);
                    } else {
                        if (!is_null($entity->id)) {
                            if(can('edit', $model->columns->where('name', $column)->first(), 'Column')) {
                                $str .= type_modifier_for_form($column, $model, $entity);
                            }
                        } else {
                            if(can('create', $model->columns->where('name', $column)->first(), 'Column')) {
                                $str .= type_modifier_for_form($column, $model, $entity);
                            }
                        }
                    }

                }

            }

            //append DB table columns with many-to-many relations
            foreach ($model->columns()->whereHas('type', function ($q) {$q->whereIn('rareadmin_types.name', ['complex_relation', 'relation', 'morph']);})->get() as $complex_relation) {

                //if is not in hidden columns or in columns as relation
                if (
                    !in_array($complex_relation->name, array_merge(['id'], explode(',', preg_replace('/\s+/', '', $model->hidden_columns))))
                    and
                    !in_array($complex_relation->name, $columns)
                ) {

                    //permission check
                    if(is_null($model->columns->where('name', $complex_relation->name)->first())) {
                        $str .= type_modifier_for_form($complex_relation->name, $model, $entity);
                    } else {
                        if (!is_null($entity->id)) {
                            if(can('edit', $model->columns->where('name', $complex_relation->name)->first(), 'Column')) {
                                $str .= type_modifier_for_form($complex_relation->name, $model, $entity);
                            }
                        } else {
                            if(can('create', $model->columns->where('name', $complex_relation->name)->first(), 'Column')) {
                                $str .= type_modifier_for_form($complex_relation->name, $model, $entity);
                            }
                        }
                    }

                }

            }

        } elseif ($for == 'table') {

            //get all table columns from DB
            $columns = \Illuminate\Support\Facades\Schema::getColumnListing($table);
            foreach ($columns as $column) {

                //if is not in hidden columns
                if (!in_array($column, array_merge(['id'], explode(',', preg_replace('/\s+/', '', $model->hidden_columns))))) {

                    //get custom name or leave as it is
                    if($modified_column = $model->columns->where('name', $column)->first()) {
                        $column_title = $modified_column->title;
                    } else {
                        $column_title = $column;
                    }

                    //permission check
                    if(is_null($model->columns->where('name', $column)->first())) {
                        $str .= '<th>' . $column_title . '</th>';
                    } elseif(can('view', $model->columns->where('name', $column)->first(), 'Column')) {
                        $str .= '<th>' . $column_title . '</th>';
                    }

                }

            }

            //append DB table columns with many-to-many relations
            foreach ($model->columns()->whereHas('type', function ($q) {$q->whereIn('rareadmin_types.name', ['complex_relation', 'relation', 'morph']);})->get() as $complex_relation) {

                //if is not in hidden columns or in columns as relation
                if (
                    !in_array($complex_relation->name, array_merge(['id'], explode(',', preg_replace('/\s+/', '', $model->hidden_columns))))
                    and
                    !in_array($complex_relation->name, $columns)
                ) {

                    //get custom name or leave as it is
                    if($modified_column = $model->columns->where('name', $complex_relation->name)->first()) {
                        $column_title = $modified_column->title;
                    } else {
                        $column_title = $complex_relation->name;
                    }

                    //permission check
                    if(is_null($model->columns->where('name', $complex_relation->name)->first())) {
                        $str .= '<th>' . $column_title . '</th>';
                    } elseif(can('view', $model->columns->where('name', $complex_relation->name)->first(), 'Column')) {
                        $str .= '<th>' . $column_title . '</th>';
                    }
                }

            }

        }

        echo $str;

    }

}

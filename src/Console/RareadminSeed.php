<?php

namespace MichalPalus1\Rareadmin\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class RareadminSeed extends Command
{
    protected $signature = 'rareadmin:seed';

    protected $description = 'Seed RareAdmin';

    public function handle()
    {
        DB::table('rareadmin_roles')->insert([
            'id' => 1,
            'name' => 'user'
        ]);

        DB::table('rareadmin_roles')->insert([
            'id' => 2,
            'name' => 'client'
        ]);

        DB::table('rareadmin_roles')->insert([
            'id' => 10,
            'name' => 'superadmin'
        ]);

//        DB::table('users')->insert([
//            'name' => 'Super Admin',
//            'email' => 'superadmin@demo.com',
//            'password' => Hash::make('password'),
//            'role_id' => 10
//        ]);
//
//        DB::table('users')->insert([
//            'name' => 'Demo User',
//            'email' => 'user@demo.com',
//            'password' => Hash::make('password'),
//            'role_id' => 1
//        ]);
//
//        DB::table('users')->insert([
//            'name' => 'Demo Client',
//            'email' => 'client@demo.com',
//            'password' => Hash::make('password'),
//            'role_id' => 2
//        ]);


        //types

        DB::table('rareadmin_types')->insert([
            'id' => 1,
            'position' => 1,
            'name' => 'text',
            'title' => 'Text',
            'form' => '<div class="form-group row">
    <label for="input_X_COLUMN_NAME_X" class="col-sm-4 col-form-label">X_COLUMN_TITLE_X</label>
    <div class="col-sm-8">
        <input type="text" name="X_COLUMN_NAME_X" class="form-control" id="input_X_COLUMN_NAME_X" value="X_VALUE_X">
    </div>
</div>'
        ]);

        DB::table('rareadmin_types')->insert([
            'id' => 2,
            'position' => 2,
            'name' => 'textarea',
            'title' => 'Large Text',
            'form' => '<div class="form-group row">
    <label for="input_X_COLUMN_NAME_X" class="col-sm-4 col-form-label">X_COLUMN_TITLE_X</label>
    <div class="col-sm-8">
        <textarea name="X_COLUMN_NAME_X" class="form-control" id="input_X_COLUMN_NAME_X">X_VALUE_X</textarea>
    </div>
</div>'
        ]);

        DB::table('rareadmin_types')->insert([
            'id' => 3,
            'position' => 8,
            'name' => 'image',
            'title' => 'Image / Images',
            'form' => '<div class="form-group row">
    <label for="input_X_COLUMN_NAME_X" class="col-sm-4 col-form-label">X_COLUMN_TITLE_X</label>
    <div class="col-sm-8">
        <input type="file" name="X_COLUMN_NAME_X[]" class="form-control" id="input_X_COLUMN_NAME_X" value="X_VALUE_X" multiple>
    </div>
</div>'
        ]);

        DB::table('rareadmin_types')->insert([
            'id' => 4,
            'position' => 10,
            'name' => 'relation',
            'title' => 'One-To-Many Relation',
            'form' => '<div class="form-group row">
    <label for="input_X_COLUMN_NAME_X" class="col-sm-4 col-form-label">X_COLUMN_TITLE_X</label>
    <div class="col-sm-8">
        <select name="X_COLUMN_NAME_X" class="form-control select2" id="input_X_COLUMN_NAME_X">
            <option selected disabled>Choose...</option>
            X_VALUE_X
        </select>
    </div>
</div>'
        ]);

        DB::table('rareadmin_types')->insert([
            'id' => 5,
            'position' => 7,
            'name' => 'boolean',
            'title' => 'True / False',
            'form' => '<div class="form-group row">
    <label for="input_X_COLUMN_NAME_X" class="col-sm-4 col-form-label">X_COLUMN_TITLE_X</label>
    <div class="col-sm-8">
        <input type="checkbox" name="X_COLUMN_NAME_X" style="width:40px" class="form-control" id="input_X_COLUMN_NAME_X" value="1" X_VALUE_X>
    </div>
</div>'
        ]);

        DB::table('rareadmin_types')->insert([
            'id' => 6,
            'position' => 9,
            'name' => 'parent',
            'title' => 'Parent',
            'form' => '<div class="form-group row">
    <label for="input_X_COLUMN_NAME_X" class="col-sm-4 col-form-label">X_COLUMN_TITLE_X</label>
    <div class="col-sm-8">
        <select name="X_COLUMN_NAME_X" class="form-control select2" id="input_X_COLUMN_NAME_X">
            <option selected disabled>Choose...</option>
            X_VALUE_X
        </select>
    </div>
</div>'
        ]);

        DB::table('rareadmin_types')->insert([
            'id' => 7,
            'position' => 11,
            'name' => 'complex_relation',
            'title' => 'Many-To-Many Relation',
            'form' => '<div class="form-group row">
    <label for="input_X_COLUMN_NAME_X" class="col-sm-4 col-form-label">X_COLUMN_TITLE_X</label>
    <div class="col-sm-8">
        <select name="X_COLUMN_NAME_X[]" class="form-control select2" id="input_X_COLUMN_NAME_X" multiple>
            <option disabled>Choose...</option>
            X_VALUE_X
        </select>
    </div>
</div>'
        ]);

        DB::table('rareadmin_types')->insert([
            'id' => 8,
            'position' => 4,
            'name' => 'code',
            'title' => 'Code editor',
            'form' => '<div class="form-group row">
    <label for="input_X_COLUMN_NAME_X" class="col-sm-4 col-form-label">X_COLUMN_TITLE_X</label>
    <div class="col-sm-8">
        <textarea name="X_COLUMN_NAME_X" class="form-control codemirror" id="input_X_COLUMN_NAME_X">X_VALUE_X</textarea>
    </div>
</div>'
        ]);

        DB::table('rareadmin_types')->insert([
            'id' => 9,
            'position' => 3,
            'name' => 'wysiwyg',
            'title' => 'Wysiwyg editor',
            'form' => '<div class="form-group row">
    <label for="input_X_COLUMN_NAME_X" class="col-sm-4 col-form-label">X_COLUMN_TITLE_X</label>
    <div class="col-sm-8">
        <textarea name="X_COLUMN_NAME_X" class="form-control summernote" id="input_X_COLUMN_NAME_X">X_VALUE_X</textarea>
    </div>
</div>'
        ]);

        DB::table('rareadmin_types')->insert([
            'id' => 10,
            'position' => 12,
            'name' => 'morph',
            'title' => 'Morph',
            'form' => '<div class="form-group row">
    <label for="input_X_COLUMN_NAME_X" class="col-sm-4 col-form-label">X_COLUMN_TITLE_X</label>
    <div class="col-sm-8">
        <select name="X_COLUMN_NAME_X" class="form-control select2" id="input_X_COLUMN_NAME_X">
            <option selected disabled>Choose...</option>
            X_VALUE_X
        </select>
    </div>
</div>'
        ]);

        DB::table('rareadmin_types')->insert([
            'id' => 11,
            'position' => 5,
            'name' => 'number',
            'title' => 'Number',
            'form' => '<div class="form-group row">
    <label for="input_X_COLUMN_NAME_X" class="col-sm-4 col-form-label">X_COLUMN_TITLE_X</label>
    <div class="col-sm-8">
        <input type="number" name="X_COLUMN_NAME_X" class="form-control" id="input_X_COLUMN_NAME_X" value="X_VALUE_X">
    </div>
</div>'
        ]);

        DB::table('rareadmin_types')->insert([
            'id' => 12,
            'position' => 6,
            'name' => 'timestamp',
            'title' => 'Timestamp',
            'form' => '<div class="form-group row">
    <label for="input_X_COLUMN_NAME_X" class="col-sm-4 col-form-label">X_COLUMN_TITLE_X</label>
    <div class="col-sm-8">
        <input type="datetime-local" name="X_COLUMN_NAME_X" class="form-control" id="input_X_COLUMN_NAME_X" value="X_VALUE_X">
    </div>
</div>'
        ]);

        DB::table('rareadmin_types')->insert([
            'id' => 13,
            'position' => 4,
            'name' => 'files',
            'title' => 'File / Files',
            'form' => '<div class="form-group row">
    <label for="input_X_COLUMN_NAME_X" class="col-sm-4 col-form-label">X_COLUMN_TITLE_X</label>
    <div class="col-sm-8">
        <input type="file" name="X_COLUMN_NAME_X[]" class="form-control" id="input_X_COLUMN_NAME_X" value="X_VALUE_X" multiple>
    </div>
</div>'
        ]);


        //entities

        DB::table('rareadmin_entities')->insert([
            'id' => 1,
            'name' => 'rareadmin_roles',
            'title' => 'Roles',
            'icon' => 'fa-user-shield'
        ]);

        DB::table('rareadmin_entities')->insert([
            'id' => 2,
            'name' => 'users',
            'title' => 'Users',
            'icon' => 'fa-users',
            'hidden_columns' => 'email_verified_at, password, remember_token, created_at, updated_at'
        ]);


        //columns

        DB::table('rareadmin_columns')->insert([
            'id' => 1,
            'entity_id' => 1,
            'name' => 'name',
            'title' => 'Name',
            'type_id' => 1
        ]);

        DB::table('rareadmin_columns')->insert([
            'id' => 2,
            'entity_id' => 2,
            'name' => 'name',
            'title' => 'Full name',
            'type_id' => 1
        ]);

        DB::table('rareadmin_columns')->insert([
            'id' => 3,
            'entity_id' => 2,
            'name' => 'email',
            'title' => 'E-mail',
            'type_id' => 1
        ]);

        DB::table('rareadmin_columns')->insert([
            'id' => 4,
            'entity_id' => 2,
            'name' => 'role_id',
            'title' => 'Role',
            'type_id' => 4,
            'viewable' => 'name'
        ]);


        //entity restrictions

        DB::table('rareadmin_restrictions')->insert([
            'target_type' => '\MichalPalus1\Rareadmin\Models\Entity',
            'target_id' => 1,
            'role_id' => 1,
            'view' => false,
            'edit' => false,
            'create' => false,
            'delete' => false
        ]);

        DB::table('rareadmin_restrictions')->insert([
            'target_type' => '\MichalPalus1\Rareadmin\Models\Entity',
            'target_id' => 2,
            'role_id' => 1,
            'view' => true,
            'edit' => false,
            'create' => false,
            'delete' => false
        ]);

        DB::table('rareadmin_restrictions')->insert([
            'target_type' => '\MichalPalus1\Rareadmin\Models\Entity',
            'target_id' => 1,
            'role_id' => 2,
            'view' => false,
            'edit' => false,
            'create' => false,
            'delete' => false
        ]);

        DB::table('rareadmin_restrictions')->insert([
            'target_type' => '\MichalPalus1\Rareadmin\Models\Entity',
            'target_id' => 2,
            'role_id' => 2,
            'view' => true,
            'edit' => true,
            'create' => true,
            'delete' => false
        ]);

        $this->info('Success');
    }
}

<?php

namespace MichalPalus1\Rareadmin;

use Illuminate\Support\Facades\Facade;

class RareadminFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'rareadmin';
    }
}

<?php

namespace MichalPalus1\Rareadmin\Models;

use Illuminate\Database\Eloquent\Model;

class Column extends Model
{
    protected $guarded = ['id'];

    protected $table = 'rareadmin_columns';

    public function entity() {
        return $this->belongsTo(Entity::class);
    }

    public function type() {
        return $this->belongsTo(Type::class);
    }

    public function restrictions() {
        return $this->morphMany(Restriction::class, 'target');
    }
}

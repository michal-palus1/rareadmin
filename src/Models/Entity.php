<?php

namespace MichalPalus1\Rareadmin\Models;

use Illuminate\Database\Eloquent\Model;

class Entity extends Model
{
    protected $table = 'rareadmin_entities';

    public function columns() {
        return $this->hasMany(Column::class);
    }

    public function restrictions() {
        return $this->morphMany(Restriction::class, 'target');
    }
}

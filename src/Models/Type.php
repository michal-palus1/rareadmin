<?php

namespace MichalPalus1\Rareadmin\Models;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    public $timestamps = false;

    protected $table = 'rareadmin_types';
}

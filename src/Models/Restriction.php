<?php

namespace MichalPalus1\Rareadmin\Models;

use Illuminate\Database\Eloquent\Model;

class Restriction extends Model
{
    public $timestamps = false;

    protected $table = 'rareadmin_restrictions';

    public function target() {
        return $this->morphTo();
    }
    public function role() {
        return $this->belongsTo(Role::class);
    }

}

<?php

namespace MichalPalus1\Rareadmin\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public $timestamps = false;

    protected $table = 'rareadmin_roles';

    protected $guarded = ['id'];

    public function users() {
        return $this->hasMany(config('auth.providers.users.model'));
    }
}
